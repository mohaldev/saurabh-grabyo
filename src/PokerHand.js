// export class PokerHand {

// 	compareWith() {
// 		return Result.TIE;
// 	}

// }

// export const Result = {
// 	WIN: 1,
// 	LOSS: 2,
// 	TIE: 3
// };

// export default PokerHand;

//Enter the cards below as player1 and player2

let player1 = "AS AH 6D AC 9D";
let player2 = "2S 10H AD 3C 10D";

var myCards = new Array;
var yourCards = new Array;

dealCards();

function dealCards() {

    let myHand = new Array;
    let player1Join = player1.split(" ")
    player1Join.forEach(element => {
        if((element === "AS") || (element === "as") || (element === "As") || (element === "aS")) {
            element = 1
            myHand.push(element);
        }
        else if((element === "AH") || (element === "ah") || (element === "Ah") || (element === "aH")) {
            element = 14
            myHand.push(element);
        }
        else if((element === "AC") || (element === "ac") || (element === "Ac") || (element === "aC")) {
            element = 27
            myHand.push(element);
        }
        else if((element === "AD") || (element === "ad") || (element === "Ad") || (element === "aD")) {
            element = 40
            myHand.push(element);
        }
        else if(element.includes("S") || element.includes("s") ) {
            let x = element.slice(0, -1)
            let y = parseInt(x)
            myHand.push(y);
        }
        else if(element.includes("H") || element.includes("h") ) {
            let x = element.slice(0, -1)
            let y = parseInt(x) + 13;
            myHand.push(y);
        }
        else if(element.includes("C") || element.includes("c") ) {
            let x = element.slice(0, -1)
            let y = parseInt(x) + 26;
            myHand.push(y);
        }
        else if(element.includes("D") || element.includes("d") ) {
            let x = element.slice(0, -1)
            let y = parseInt(x) + 39;
            myHand.push(y);
        }
    })

    let yourHand = new Array;
    let player2Join = player2.split(" ")
    player2Join.forEach(element => {
        if((element === "AS") || (element === "as") || (element === "As") || (element === "aS")) {
            element = 1
            yourHand.push(element);
        }
        else if((element === "AH") || (element === "ah") || (element === "Ah") || (element === "aH")) {
            element = 14
            yourHand.push(element);
        }
        else if((element === "AC") || (element === "ac") || (element === "Ac") || (element === "aC")) {
            element = 27
            yourHand.push(element);
        }
        else if((element === "AD") || (element === "ad") || (element === "Ad") || (element === "aD")) {
            element = 40
            yourHand.push(element);
        }
        else if(element.includes("S") || element.includes("s") ) {
            let x = element.slice(0, -1)
            let y = parseInt(x)
            yourHand.push(y);
        }
        else if(element.includes("H") || element.includes("h") ) {
            let x = element.slice(0, -1)
            let y = parseInt(x) + 13;
            yourHand.push(y);
        }
        else if(element.includes("C") || element.includes("c") ) {
            let x = element.slice(0, -1)
            let y = parseInt(x) + 26;
            yourHand.push(y);
        }
        else if(element.includes("D") || element.includes("d") ) {
            let x = element.slice(0, -1)
            let y = parseInt(x) + 39;
            yourHand.push(y);
        }
    })
    
    playerOneArray = new Array;
    playerTwoArray = new Array;

	//create objects for cards with their value and suit properties
	for (i = 0; i <= 4; i++) {
		if (myHand[i] <= 13) {
			cardValue = myHand[i];
            myCards[i] = {value: cardValue, suit: "S"};
            playerOneArray.push(cardValue + myCards[i].suit)
		} else if ((myHand[i] > 13) && (myHand[i] <= 26)) {
			cardValue = myHand[i] - 13;
            myCards[i] = {value: cardValue, suit: 'H'};
            playerOneArray.push(cardValue + myCards[i].suit)
		} else if ((myHand[i] > 26) && (myHand[i] <= 39)) {
			cardValue = myHand[i] - 26;
            myCards[i] = {value: cardValue, suit: 'C'};
            playerOneArray.push(cardValue + myCards[i].suit)
		} else {
			cardValue = myHand[i] - 39;
            myCards[i] = {value: cardValue, suit: 'D'};
            playerOneArray.push(cardValue + myCards[i].suit)
		}
		if (yourHand[i] <= 13) {
			cardValue = yourHand[i];
            yourCards[i] = {value: cardValue, suit: "S"};
            playerTwoArray.push(cardValue + yourCards[i].suit)
		} else if ((yourHand[i] > 13) && (yourHand[i] <= 26)) {
			cardValue = yourHand[i] - 13;
            yourCards[i] = {value: cardValue, suit: 'H'};
            playerTwoArray.push(cardValue + yourCards[i].suit)
		} else if ((yourHand[i] > 26) && (yourHand[i] <= 39)) {
			cardValue = yourHand[i] - 26;
            yourCards[i] = {value: cardValue, suit: 'C'};
            playerTwoArray.push(cardValue + yourCards[i].suit)
		} else {
			cardValue = yourHand[i] - 39;
            yourCards[i] = {value: cardValue, suit: 'D'};
            playerTwoArray.push(cardValue + yourCards[i].suit)
		}        
    }
	console.log("PlayerOne " + playerOneArray.join(" "))
	document.getElementById("OneCards").innerHTML = "PlayerOne " + playerOneArray.join(" ")
	console.log("PlayerTwo " + playerTwoArray.join(" "))
	document.getElementById("TwoCards").innerHTML = "PlayerTwo " + playerTwoArray.join(" ")

evaluateHand();

}

//This function evaluates the hands and ouputs the winner and score

function evaluateHand() {
	var i, j;
	var myPoints = 0;
	var yourPoints = 0;
	//sort cards by value
	yourCards.sort(function(a, b) {
		if (a.value < b.value) return -1;
		if (a.value > b.value) return 1;		
		return 0;
	});
	myCards.sort(function(a, b) {
		if (a.value < b.value) return -1;
		if (a.value > b.value) return 1;		
		return 0;
	});
	//
	//Make aces high if not needed for a low run - first for myCards
	//
	if ((myCards[0].value !== 1) || (myCards[1].value !== 2) || (myCards[2].value !== 3) || (myCards[3].value !== 4) || (myCards[4].value !== 5)) { 	//if not a low run...
		for (i = 0; i <=4; i++) {			//loop through the hand
			if (myCards[i].value === 1) { 	//if a card is an ace
				myCards[i].value = 14;		//then make it one above a king
			}
		}
		myCards.sort(function(a, b) {		//sort the cards again
			if (a.value < b.value) return -1;
			if (a.value > b.value) return 1;		
			return 0;
		});
	}
	//
	//Now make aces high if not needed for a low run - now for yourCards
	//
	if ((yourCards[0].value !== 1) || (yourCards[1].value !== 2) || (yourCards[2].value !== 3) || (yourCards[3].value !== 4) || (yourCards[4].value !== 5)) { //if not a low run...
		for (i = 0; i <=4; i++) {							//loop through the hand
			if (yourCards[i].value === 1) { 				//if a card is an ace
				yourCards[i].value = 14;					//then make it one above a king
			}
		}
		yourCards.sort(function(a, b) {						//sort the cards again
			if (a.value < b.value) return -1;
			if (a.value > b.value) return 1;		
			return 0;
		});
	}
	for (i = 0; i <=4; i++) {
	}
	
	//Check for a flush
	
	for (i = 1; i <=4; i++) {									//Go through loop and see if each suit matches the first one
		j = i - 1;
		if (myCards[i].suit !== myCards[j].suit) break;
		var myFlushCounter = i;
	}
	if (myFlushCounter === 4) {
        myPoints = myPoints + 500 + myCards[4].value;			//500 points for a flush, add high card value to points
		console.log("PlayerOne " + "Flush");
		document.getElementById("OneReason").innerHTML = "PlayerOne " + "Flush";
	}
	for (i = 1; i <=4; i++) {									//Go through loop and see if each suit matches the first one
		j = i - 1;
		if (yourCards[i].suit !== yourCards[j].suit) break;
		var yourFlushCounter = i;
	}
	if (yourFlushCounter === 4) {
        yourPoints = yourPoints + 500 + yourCards[4].value;		//500 points for a flush, add high card value to points
		console.log("PlayerTwo " + "Flush ");
		document.getElementById("TwoReason").innerHTML = "PlayerTwo " + "Flush";
	}
	
	//Check for a straight
	
	for (i = 1; i <=4; i++) {									//Go through loop and subtract values
		j = i - 1;
		var difference = myCards[i].value - myCards[j].value;
		if (difference !== 1) break;
		var myStraightCounter = i;
	}
	if (myStraightCounter === 4) {
		myPoints = myPoints + 400 + myCards[4].value;			//400 points for a straight, add high card value to points
		console.log("PlayerOne " + "Straight ");
		document.getElementById("OneReason").innerHTML = "PlayerOne " + "Straight";
		if (myPoints < 500) {
			console.log("PlayerOne " + '- ' + " high");
			document.getElementById("OneReason").innerHTML = "PlayerOne " + "-" + " high";
		}
	}
	for (i = 1; i <=4; i++) {									//Go through loop and subtract values
		j = i - 1;
		difference = yourCards[i].value - yourCards[j].value;
		if (difference !== 1) break;
		var yourStraightCounter = i;
	}
	if (yourStraightCounter === 4) {
		yourPoints = yourPoints + 400 + yourCards[4].value;		
		console.log("PlayerTwo " + "Straight ");
		document.getElementById("TwoReason").innerHTML = "PlayerTwo " + "Straight";
		if (yourPoints < 500) {
			console.log("PlayerTwo " + '- ' + " high");
			document.getElementById("TwoReason").innerHTML = "PlayerTwo " + "High";
		}
	}
	
	//Check for two, three, and four-of-a-kind
	
	if (myPoints < 400) {											//don't try this if there is already a flush or straight
		var myHiCard;
		var myPairsArray = [];
		for (i = 1; i <=4; i++) {									//Go through loop and subtract values
			j = i - 1;
			difference = myCards[i].value - myCards[j].value;
			if (difference === 0) {
				myPairsArray[j] = 1;
				myHiCard = myCards[i].value;
			} else {
				myPairsArray[j] = 0;
			}
		}
		var myPairsTotal = 0;											//total up the points for the pairs
		for (i = 0; i <= 3; i++) {
			myPairsTotal = myPairsTotal + myPairsArray[i];
		}
		if (myPairsTotal === 1) {										//Check for pairs
            myPoints = myPoints + 100 + myHiCard;
			console.log("PlayerOne " + "A pair");
			document.getElementById("OneReason").innerHTML = "PlayerOne " + "A Pair";
		}
		if (myPairsTotal === 0) {										//If nothing else, high card is used
            myPoints = myPoints + myCards[4].value;
			console.log("PlayerOne " + "A single ")
			document.getElementById("OneReason").innerHTML = "PlayerOne " + "A single";
		}
		if ((myPairsTotal === 3) && (myPairsArray[1] === 0 || myPairsArray[2] === 0)) {	//full house
            myPoints = myPoints + 600 + myCards[4].value;
			console.log("PlayerOne " + "Full-house");
			document.getElementById("OneReason").innerHTML = "PlayerOne " + "Full-house";
		}
		if ((myPairsTotal === 3) && (myPairsArray[0] === 0 || myPairsArray[3] === 0)) {	
            myPoints = myPoints + 700 + myCards[3].value;								//four-of-a-kind
			console.log("PlayerOne " + "Four-of-a-kind");
			document.getElementById("OneReason").innerHTML = "PlayerOne " + "Four-of-a-kind";
			
		}
		if (myPairsTotal === 2) {										//Check for two pairs or 3 of a kind
			if ((myPairsArray[0] === 1 && myPairsArray[1] === 1) || (myPairsArray[1] === 1 && myPairsArray[2] ===1) || (myPairsArray[2] === 1 && myPairsArray[3] ===1)) {
                myPoints = myPoints + 300 + myCards[2].value;
				console.log("PlayerOne " + "Three-of-a-kind");
				document.getElementById("OneReason").innerHTML = "PlayerOne " + "Three-of-a-kind";
			} else {
                myPoints = myPoints + 200 + myCards[3].value;
				console.log("PlayerOne " + "Two pairs");
				document.getElementById("OneReason").innerHTML = "PlayerOne " + "Two-pairs";
			}
		}
	}
	if (yourPoints < 400) {												//don't try this if there is already a flush or straight
		var yourHiCard;
		var yourPairsArray = [];
		for (i = 1; i <=4; i++) {										//Go through loop and subtract values
			j = i - 1;
			difference = yourCards[i].value - yourCards[j].value;
			if (difference === 0) {
				yourPairsArray[j] = 1;
				yourHiCard = yourCards[i].value;
			} else {
				yourPairsArray[j] = 0;
			}
		}
		var yourPairsTotal = 0;											//total up the points for the pairs
		for (i = 0; i <= 3; i++) {
			yourPairsTotal = yourPairsTotal + yourPairsArray[i];
		}
			if (yourPairsTotal === 1) {									//Check for pairs
            yourPoints = yourPoints + 100 + yourHiCard;
			console.log("PlayerTwo " + "A pair ");
			document.getElementById("TwoReason").innerHTML = "PlayerTwo " + "A pair";
		}
			if (yourPairsTotal === 0) {									//if nothing else, high card is used
            yourPoints = yourPoints + yourCards[4].value;
			console.log("PlayerTwo " + "Full-house");
			document.getElementById("TwoReason").innerHTML = "PlayerTwo " + "Full-house";
		}
		if ((yourPairsTotal === 3) && (yourPairsArray[1] === 0 || yourPairsArray[2] === 0)) {	//full house
            yourPoints = yourPoints + 600 + yourCards[4].value;
            console.log("PlayerTwo " + "Full-house");
			document.getElementById("TwoReason").innerHTML = "PlayerTwo " + "Full-house";
            
		}
		if ((yourPairsTotal === 3) && (yourPairsArray[0] === 0 || yourPairsArray[3] === 0)) {
            yourPoints = yourPoints + 700 + yourCards[3].value;									//four-of-a-kind
            console.log("PlayerTwo " + "Four-of-a-kind");
			document.getElementById("TwoReason").innerHTML = "PlayerTwo " + "Four-of-a-kind";
		}
		if (yourPairsTotal === 2) {										//Check for two pairs or 3 of a kind
			if ((yourPairsArray[0] === 1 && yourPairsArray[1] === 1) || (yourPairsArray[1] === 1 && yourPairsArray[2] ===1) || (yourPairsArray[2] === 1 && yourPairsArray[3] ===1)) {
                yourPoints = yourPoints + 300 + yourCards[2].value;
				console.log("PlayerTwo " + "Three-of-a-kind");
				
			document.getElementById("TwoReason").innerHTML = "PlayerTwo " + "Three-of-a-kind";
			} else {
                yourPoints = yourPoints + 200 + yourCards[3].value;
                console.log("PlayerTwo " + "Two pairs");
				document.getElementById("TwoReason").innerHTML = "PlayerTwo " + "Two pairs";
			}
		}
	}
	if ((myPairsTotal === 0) && (yourPairsTotal === 0) && (myPoints === yourPoints)) {	//break a single card tie with next ranking high cards
		for (i = 3; i >= 0; i--) {
			myPoints = myPoints + myCards[i].value;
			yourPoints = yourPoints + yourCards[i].value;
			if (myPoints !== yourPoints) break;
		}
	}
	if ((myPairsTotal === 1) && (yourPairsTotal === 1) && (myPoints === yourPoints)) {	//break a pairs tie with next ranking high cards
		for (i = 4; i >= 0; i--) {
			myPoints = myPoints + myCards[i].value;
			yourPoints = yourPoints + yourCards[i].value;
			if (myPoints !== yourPoints) break;
		}
	}		
	if (myPoints > yourPoints) {
		myResult = '- 1 (WIN)';
		yourResult = '- 2 (LOSS)';
	}
	if (myPoints < yourPoints) {
		yourResult = '- 1 (WIN)';
		myResult = '- 2 (LOSS)';
		}
	if (myPoints === yourPoints) {
		yourResult = '- 3 (TIE)';
		myResult = '- 3 (TIE)';
	}
	if (yourPoints === 928) {
		document.getElementById("OneRF").innerHTML = "PlayerOne = Royal Flush"
        console.log("PlayerTwo = Royal Flush")
	}	
	if (myPoints === 928) {
		document.getElementById("TwoRF").innerHTML = "PlayerTwo = Royal Flush"
        console.log("PlayerOne = Royal Flush")
	}	
	// console.log('Player1 ' + myPoints + myResult);
    // console.log('Player2 '+ yourPoints + yourResult);
	
	document.getElementById("One").innerHTML = 'PlayerOne ' + myResult;
	document.getElementById("Two").innerHTML = 'PlayerTwo ' + yourResult;
    console.log('PlayerOne ' + myResult);
	console.log('PlayerTwo '+ yourResult);
}

// Royal Flush = 928 (Flush + Straight + 2 x ace)
// Four of a kind = 700
// Full House = 600
// Flush = 500 
// Straight = 400 
// Three of a kind = 300
// Two pairs = 200
// Pair = 100
// Highcard = max 14